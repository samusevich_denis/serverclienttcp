﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class Client : MonoBehaviour
{
    private Coroutine coroutineUDPServer;

    [SerializeField] private int portSend;
    [SerializeField] private int portRecive;
    Thread getIPAddressClientThread;
    Thread getSendIPAddress;
    private void Start()
    {
        SetSettingConnect();
    }

    private void SetSettingConnect()
    {
        NetworkUDPClient.Init(portRecive, portSend);
        getIPAddressClientThread = new Thread(new ThreadStart(NetworkUDPClient.GetIPAddressClient));
        getIPAddressClientThread.Start();
        getSendIPAddress = new Thread(new ThreadStart(NetworkUDPClient.SendIPAddress));
        getSendIPAddress.Start();
        coroutineUDPServer = StartCoroutine(WaitConect());
    }

    private IEnumerator WaitConect()
    {
        while (!NetworkUDPClient.isConect)
        {
            yield return null;
        }
        Debug.Log(NetworkUDPClient.IPAddressClient.ToString());
        yield return new WaitForSeconds(2);
        NetworkUDPClient.Close();
        ConnectTCP();
    }

    private void OnDestroy()
    {
        NetworkUDPClient.Close();
        if (coroutineUDPServer != null)
        {
            StopCoroutine(coroutineUDPServer);
        }
    }

    private void ConnectTCP()
    {
        NetworkTCPClient.StartClient(NetworkUDPClient.IPAddressClient);
    }

    public void SendMes1()
    {
        var msg = ServerMessageHandler.GetMessage(TypeMessageToServer.Data);
        NetworkTCPClient.SendMessageToServer(msg);
    }
    public void SendMes2()
    {
        var msg = ServerMessageHandler.GetMessage(TypeMessageToServer.Video);
        NetworkTCPClient.SendMessageToServer(msg);
    }
    public void SendMes3()
    {
        var msg = ServerMessageHandler.GetMessage(TypeMessageToServer.Callback);
        NetworkTCPClient.SendMessageToServer(msg);
    }
}
