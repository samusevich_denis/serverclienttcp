﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Server : MonoBehaviour
{
    private Coroutine coroutineUDPServer;
    [SerializeField] private int portSend;
    [SerializeField] private int portRecive;
    Thread getIPAddressClientThread;
    Thread getSendIPAddress;

    private void Start()
    {
        SetSettingConnect();
    }

    private void SetSettingConnect()
    {
        NetworkUDPClient.Init(portRecive, portSend);
        getIPAddressClientThread = new Thread(new ThreadStart(NetworkUDPClient.GetIPAddressClient));
        getIPAddressClientThread.Start();
        getSendIPAddress = new Thread(new ThreadStart(NetworkUDPClient.SendIPAddress));
        getSendIPAddress.Start();
        coroutineUDPServer = StartCoroutine(WaitConect());
    }

    private IEnumerator WaitConect()
    {
        while (!NetworkUDPClient.isConect)
        {
            yield return null;
        }
        Debug.Log(NetworkUDPClient.IPAddressClient.ToString());
        yield return new WaitForSeconds(2);
        NetworkUDPClient.Close();
        ConnectTCP();
    }

    private void OnDestroy()
    {
        NetworkUDPClient.Close();
        if (coroutineUDPServer!= null)
        {
            StopCoroutine(coroutineUDPServer);
        }
    }

    private void ConnectTCP()
    {
        NetworkTCPServer.StarServer();
    }

    public void SendMes1()
    {
        var msg = ClientMessageHandler.GetMessage(TypeMessageToClient.DataCl);
        NetworkTCPServer.SendToClientMessage(msg);
    }

    public void SendMes2()
    {
        var msg = ClientMessageHandler.GetMessage(TypeMessageToClient.VideoCl);
        NetworkTCPServer.SendToClientMessage(msg);
    }
    public void SendMes3()
    {
        var msg = ClientMessageHandler.GetMessage(TypeMessageToClient.CallbackCl);
        NetworkTCPServer.SendToClientMessage(msg);
    }
}
