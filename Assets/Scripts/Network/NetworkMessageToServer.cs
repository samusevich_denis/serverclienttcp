﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum TypeMessageToServer
{
    Data,
    Video,
    Callback,
}

[Serializable]
public class NetworkMessageToServer
{
    public TypeMessageToServer Type;
    public string Data;

    public NetworkMessageToServer(TypeMessageToServer type, string data)
    {
        Type = type;
        Data = data;
    }
}
