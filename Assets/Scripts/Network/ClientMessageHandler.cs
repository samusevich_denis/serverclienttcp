﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientMessageHandler : MonoBehaviour
{
    private static List<string> messageToClients = new List<string>();
    public static void AddMessage(string message)
    {
        messageToClients.Add(message);
    }
    private void Update()
    {
        for (int i = 0; i < messageToClients.Count; i++)
        {
            HandlMessage(messageToClients[i]);
            messageToClients.RemoveAt(i);
            i--;
        }
    }

    private static void HandlMessage(string msg)
    {
        Debug.LogWarning(msg);
        var nmsg = JsonUtility.FromJson<NetworkMessageToClient>(msg);
        switch (nmsg.Type)
        {
            case TypeMessageToClient.DataCl:
                //var dataQuestion = JsonUtility.FromJson<DataObject>(nmsg.Data);
                //ClientManager.Instance.ShowdQuestion(dataQuestion);
                break;
            default:
                break;
        }
    }

    public static string GetMessage(DataObject dataObject, TypeMessageToClient type = TypeMessageToClient.DataCl)
    {
        var data = JsonUtility.ToJson(dataObject);
        var nmsg = new NetworkMessageToClient(type, data);
        var msg = JsonUtility.ToJson(nmsg);
        return msg;
    }

    public static string GetMessage(TypeMessageToClient type)
    {
        var data = string.Empty;
        var nmsg = new NetworkMessageToClient(type, data);
        var msg = JsonUtility.ToJson(nmsg);
        return msg;
    }
}
