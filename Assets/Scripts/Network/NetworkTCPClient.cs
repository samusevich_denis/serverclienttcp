﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public static class NetworkTCPClient
{
    public static NetworkStream Stream { get; private set; }
    public static TcpClient tcpClient;
    //public NetworkClient(TcpClient tcpClient)
    //{
    //    this.tcpClient = tcpClient;
    //}

    //public NetworkClient() { }

    public static async void ReceiveMessagesForServer()
    {
        try
        {
            Stream = tcpClient.GetStream();
            string msg;
            while (true)
            {
                await Task.Yield();
                msg = GetMessage();
                if (msg.Equals(string.Empty, StringComparison.OrdinalIgnoreCase))
                {
                    continue;
                }
                ServerMessageHandler.AddMessage(msg);
            }
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.Message);
        }
        finally
        {
            //ServerMessageHandler.AddMessage(ServerMessageHandler.GetMessage(Player, TypeMessageToServer.RemovePlayerData));
            NetworkTCPServer.Close();

        }
    }

    public static void Close()
    {
        if (Stream != null)
        {
            Stream.Close();
            Stream = null;
        }
        if (tcpClient != null)
        {
            tcpClient.Close();
            tcpClient = null;
        }
    }
    private static string GetMessage()
    {
        StringBuilder builder = new StringBuilder();
        byte[] data = new byte[64];
        int bytes = 0;
        do
        {
            bytes = Stream.Read(data, 0, data.Length);
            builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
        }
        while (Stream.DataAvailable);
        return builder.ToString();
    }




    public static void StartClient(IPAddress address)
    {
        try
        {
            tcpClient = new TcpClient();
            tcpClient.Connect(address, NetworkTCPServer.port);
            Thread receiveThread = new Thread(new ThreadStart(ReceiveMessagesForClient));
            receiveThread.Start();
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.Message);
            Close();
        }
    }


    public static void SendMessageToServer(string msg)
    {

        try
        {
            byte[] data = Encoding.Unicode.GetBytes(msg);
            Stream.Write(data, 0, data.Length);
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.Message);
            Close();
        }
    }

    private static void ReceiveMessagesForClient()
    {
        try
        {
            Stream = tcpClient.GetStream();
            string msg;
            while (true)
            {
                msg = GetMessage();
                if (msg.Equals(string.Empty, StringComparison.OrdinalIgnoreCase))
                {
                    continue;
                }
                ClientMessageHandler.AddMessage(msg);
            }

        }
        catch (Exception ex)
        {
            Debug.LogError(ex.Message);
        }
        finally
        {
            Close();
        }
    }
}
