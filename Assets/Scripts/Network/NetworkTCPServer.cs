﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public static class NetworkTCPServer
{
    
    private static TcpListener tcpListener;
    //private static bool isCloseConnect = true;


    public static int port { get => 51113; }
    public static async void StarServer()
    {
        tcpListener = new TcpListener(IPAddress.Any, port);
        tcpListener.Start();
        //isCloseConnect = false;
        try
        {
            AddClient(await tcpListener.AcceptTcpClientAsync());
        }
        finally
        {
            Close();
        }
    }

    private static async void AddClient(TcpClient client)
    {
        await Task.Yield();
        NetworkTCPClient.tcpClient = client;
        Thread clientThread = new Thread(new ThreadStart(NetworkTCPClient.ReceiveMessagesForServer));
        clientThread.Start();
    }

    //public static void RemoveClient(string playerName)
    //{
        //var client = clients.FirstOrDefault(c => c.Player.Name == playerName);
        //if (client != null)
        //    clients.Remove(client);
    //}

    //public static void SendAllClientsMessage(string message)
    //{
    //    byte[] data = Encoding.Unicode.GetBytes(message);
    //    for (int i = 0; i < clients.Count; i++)
    //    {
    //        clients[i].Stream.Write(data, 0, data.Length);
    //    }
    //}
    //public static void SendAllClientsMessageIgnoringPlayers(string message, params string[] namesIgnor)
    //{
    //    if (namesIgnor.Length == 1 && namesIgnor[0].Equals(string.Empty, System.StringComparison.OrdinalIgnoreCase))
    //    {
    //        SendAllClientsMessage(message);

    //    }
    //    else
    //    {
    //        byte[] data = Encoding.Unicode.GetBytes(message);
    //        bool IsIgnor;
    //        for (int i = 0; i < clients.Count; i++)
    //        {
    //            IsIgnor = false;
    //            for (int j = 0; j < namesIgnor.Length; j++)
    //            {
    //                if (clients[i].Player.Name.Equals(namesIgnor[j], System.StringComparison.OrdinalIgnoreCase))
    //                {
    //                    IsIgnor = true;
    //                }
    //            }
    //            if (IsIgnor)
    //            {
    //                continue;
    //            }
    //            //Debug.Log($"Send {clients[i].Player.Name} ");
    //            clients[i].Stream.Write(data, 0, data.Length);
    //        }
    //    }
    //}

    public static void SendToClientMessage(string message)
    {
        try
        {
            byte[] data = Encoding.Unicode.GetBytes(message);
            NetworkTCPClient.Stream.Write(data, 0, data.Length);
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.Message);
            Close();
        }

    }
    public static string GetLocalIPAddress()
    {
        IPHostEntry host;
        string localIP = "";
        host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (IPAddress ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
            {
                localIP += ip.ToString();
            }
        }
        return localIP;
    }

    public static void Close()
    {
        tcpListener.Stop();
        NetworkTCPClient.Close();
    }

}
