﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerMessageHandler : MonoBehaviour
{
    private static List<string> messageToServers = new List<string>();
    public static void AddMessage(string message)
    {
        messageToServers.Add(message);
    }

    private void Update()
    {
        for (int i = 0; i < messageToServers.Count; i++)
        {
            HandlMessage(messageToServers[i]);
            messageToServers.RemoveAt(i);
            i--;
        }
    }


    private static void HandlMessage(string msg)
    {
        Debug.LogWarning(msg);
        var nmsg = JsonUtility.FromJson<NetworkMessageToServer>(msg);
        switch (nmsg.Type)
        {
            case TypeMessageToServer.Data:
                //var dataObject = JsonUtility.FromJson<DataObject>(nmsg.Data);
                //AdminManager.Instance.PlayerHundler.AddPlayer(player);
                break;
            default:
                break;
        }
    }

    public static string GetMessage(DataObject dataObject, TypeMessageToServer type = TypeMessageToServer.Data)
    {
        var data = JsonUtility.ToJson(dataObject);
        var nmsg = new NetworkMessageToServer(type, data);
        var msg = JsonUtility.ToJson(nmsg);
        return msg;
    }

    public static string GetMessage(TypeMessageToServer type = TypeMessageToServer.Data)
    {
        var data = string.Empty;
        var nmsg = new NetworkMessageToServer(type, data);
        var msg = JsonUtility.ToJson(nmsg);
        return msg;
    }
}

public class DataObject
{
    //string data
    //float data
}
