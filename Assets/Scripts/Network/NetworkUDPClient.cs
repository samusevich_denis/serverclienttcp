﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public static class NetworkUDPClient
{
    public static int portForResiver;
    public static int portForSender;
    public static UdpClient sender;
    public static UdpClient receiver;
    public static IPAddress IPAddressClient { get; private set; }
    public static IPAddress IPAddressServer { get; private set; }
    private static bool isSetIPAddressClient;
    private static bool isClientConect;
    public static bool isConect { get => isClientConect; }
    private static bool isStop;

    public static void Init(int portResive,int portSend)
    {
        var iPAddress = NetworkTCPServer.GetLocalIPAddress();
        IPAddressClient = null;
        IPAddressServer = IPAddress.Parse(iPAddress);
        portForResiver = portResive;
        portForSender = portSend;
        isSetIPAddressClient = false;
        isClientConect = false;
        isStop = false;
    }

    public static void GetIPAddressClient()
    {
        try
        {
            receiver = new UdpClient(portForResiver);
            IPEndPoint remoteIp = null;
            while (!isStop)
            {
                byte[] data = receiver.Receive(ref remoteIp);
                string msg = Encoding.Unicode.GetString(data);
                if (msg.Equals(string.Empty, StringComparison.OrdinalIgnoreCase))
                {
                    continue;
                }

                if (remoteIp!=null && !isSetIPAddressClient)
                {
                    IPAddressClient = remoteIp.Address;
                    isSetIPAddressClient = true;
                }
                if (msg.Equals("Подключено", StringComparison.OrdinalIgnoreCase))
                {
                    Debug.LogWarning("Подключено");
                    isClientConect = true;
                    break;
                }
            }
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.Message);

        }
        finally
        {
            CloseReceiver();
        }
    }

    private static void CloseReceiver()
    {
        if (receiver != null)
        {
            receiver.Close();
            receiver = null;
        }
    }

    public static void SendIPAddress()
    {

        try
        {
            sender = new UdpClient();
            sender.Connect(new IPEndPoint(IPAddress.Broadcast, portForSender));
            while (!isStop)
            {
                Debug.Log($"jnghfdrf_ {isSetIPAddressClient}______{isClientConect}");
                string message = isSetIPAddressClient?"Подключено":"Подключение";
                byte[] data = Encoding.Unicode.GetBytes(message);
                sender.Send(data, data.Length);
            }
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.Message);
        }
        finally
        {
            CloseSender();
        }
    }

    private static void CloseSender()
    {
        if (sender != null)
        {
            sender.Close();
            sender = null;
        }
    }
    
    public static void Close()
    {
        isStop = true;
    }
}