﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class NetworkMessageToClient
{
    public TypeMessageToClient Type;
    public string Data;

    public NetworkMessageToClient(TypeMessageToClient type, string data)
    {
        Type = type;
        Data = data;
    }
}

public enum TypeMessageToClient
{
    DataCl,
    VideoCl,
    CallbackCl,
}
